# PyAster

A package to count print nodes in Python Abstract Syntax Trees (ASTs).

Find all occurrences of 'print' string in this package:

     $ grep print pyaster --recursive --binary-files=without-match
     pyaster/prettyprint.py:"""Pretty printing with soft tabs and newlines for ASTs"""
     pyaster/prettyprint.py:def pprint_node(node, body_lines=1, indent_with='    '):
     ...

Find the number of occurences print strings:

     $ grep print pyaster --recursive --binary-files=without-match | wc -l
     26

Find all python print nodes in this package:

     $ python -c "import ast, os, pprint, pyaster; [(f, pyaster.getline(n)) for f,n in pyaster.find_in_directory(ast.Print(), os.getcwd())])"
     [('pyaster/search.py',
       "        print 'Got call to {0} at {1}'.format(func_name, print_node(node))\n"),
      ('pyaster/helpers.py',
       "        print red('Could not find class or function name for node. {0}'.format(node))\n"),
      ('pyaster/helpers.py', '        print debug(node)\n')]


# Testing

For now use doctest for testing:

    python -m doctest *.py

# To Do Perhaps

Proper packaging (deck of Hollerith punchcards), Sphinx docs, complete coverage

A good shorthand for defining searchs

Source control integration

pprint node._attributes and things to capture

Node transforms replace

Out of order lines

Trace assignment inspect/live code objects?