from setuptools import setup, find_packages

setup(
    name = "pyaster",
    version = "0.0.1",
    packages = find_packages(),

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = ['pygraphviz'],

    # package_data = {
    # # If any package contains *.txt or *.rst files, include them:
    # '': ['*.txt', '*.rst'],
    # # And include any *.msg files found in the 'hello' package, too:
    # 'hello': ['*.msg'],
    # },

    # metadata for upload to PyPI
    # author = "Me",
    # author_email = "me@example.com",
    # description = "This is an Example Package",
    # license = "PSF",
    # keywords = "hello world example examples",
    # url = "http://example.com/HelloWorld/",   # project home page, if any
    )
