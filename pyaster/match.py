"""
Uses pattern, string order of the re module

__cmp__
Comparators of Eq Typeclass have == and !=
"""

import ast

from visitors import itervisit
from capture import capture


def nodes_match(pattern, node):
    """Default node comparision should agree on node type, level, and fields

    """
    have_match = node_types_match(pattern, node) and \
            node_fields_match(pattern, node)

    # Nodes only have level and stack from itervisit
    if hasattr(pattern, 'level') and hasattr(node, 'level'):
         have_match = have_match and node_levels_match(pattern, node)

    if hasattr(pattern, 'stack') and hasattr(node, 'stack'):
        have_match = have_match and node_stacks_match(pattern, node)

    return have_match


def node_types_match(node_A, node_B):
    """Takes two AST nodes and returns whether they're the same type.

    Two nodes of the same type match:

    >>> node_types_match(ast.Name(), ast.Name())
    True

    Different nodes types do not match:

    >>> node_types_match(ast.Store(), ast.Load())
    False
    """
    return type(node_A) == type(node_B)


def node_levels_match(node_A, node_B):
    """Takes two AST nodes and returns whether they're the same distance from
    the root.

    True on same level
    >>> node_levels_match(ast.Name(level=0), ast.Module(level=0))
    True

    False on different
    >>> node_levels_match(ast.Name(level=3), ast.Module(level=0))
    False

    Assuming we get them from an iterator that assigns level:

    >>> node_levels_match(ast.Name(), ast.Expr())
    Traceback (most recent call last):
    ...
    AttributeError: 'Name' object has no attribute 'level'
    """
    return node_A.level == node_B.level


def node_stacks_match(pattern, match, match_method=nodes_match):
    """Check that two nodes have the similar stacks

    Note: not commutative a `stack_eq` b != b `stack_eq` a

    >>> no_stack = ast.Name(level=2, stack=[])
    >>> short_stack = ast.Name(level=2, \
    stack=[ast.Module(level=0), ast.Expr(level=1)])
    >>> node_stacks_match(short_stack, no_stack)
    False
    >>> node_stacks_match(no_stack, short_stack)
    False
    >>> node_stacks_match(short_stack, short_stack)
    True
    """

    for pattern_node, match_node in map(None, pattern.stack, match.stack):

        if pattern_node and not match_node:
            return False

        if not match_method(pattern_node, match_node):
            return False

    return True


def node_fields_match(pattern, node):
    """Checks if a pattern node's fields and value match a node.

    Note: not commutative a `field_eq` b != b `field_eq` a

    Match an identical node:
    >>> node_fields_match(ast.Name(id='A'),  ast.Name(id='A'))
    True

    Match against a node with fewer fields:
    >>> node_fields_match(ast.Name(), ast.Name(id='B'))
    True

    No match against a node with an extra field:
    >>> node_fields_match(ast.Name(id='B'), ast.Name())
    False

    No match if a field value is not equal:
    >>> node_fields_match(ast.Name(id='A'), ast.Name(id='B'))
    False

    Assuming we'll visit children so, ignore fields with ASTs or list values:
    >>> name_A = ast.Name(ctx=ast.Load())
    >>> name_B = ast.Name(id='ocelote', ctx=ast.Load())
    >>> node_fields_match(name_A, name_B)
    True
    >>> node_fields_match(ast.Module(body=[]), ast.Module(body=[ast.Expr()]))
    True
    """

    for field, match_value in ast.iter_fields(pattern):

        # doesn't have the field to match
        if not hasattr(node, field):
            return False

        # ignore class/function bodies and children
        if isinstance(match_value, list) or isinstance(match_value, ast.AST):
            # skip children to match later
            continue

        node_value = getattr(node, field)

        if match_value != node_value:  # field values do not match
            return False

    return True


def match_tree(pattern, tree, match_on=nodes_match):
    """Takes an tree and a skeleton AST and checks if the tree matches the
    skeleton tree using the match_on comparision function.

    Match exact trees:
    >>> match_tree(ast.parse("lambda x: x"), ast.parse("lambda x: x"))
    True
    >>> match_tree(ast.parse("lambda x: y"), ast.parse("lambda x: x"))
    False

    Match after extra intervening nodes:

    >>> tree = ast.parse("def foo(): print 1; print 'dog';").body[0]
    >>> pattern = ast.parse("def foo(): pass").body[0]
    >>> pattern.body = [] # drop the pass statement
    >>> match_tree(pattern, tree)
    True
    >>> # but not when we want to match additional nodes
    >>> match_tree(tree, pattern)
    False

    Match if the level the top node:
    >>> tree = ast.parse("lambda x: x")
    >>> tree.level = -3  # like we're starting three nodes down
    >>> match_tree(ast.parse("lambda x: x"), tree)
    True

    Includes stack the tree node:
    >>> tree = ast.parse("lambda x: x")
    >>> tree.stack = [ast.Name(level=3)]  # starting three nodes in
    >>> match_tree(ast.parse("lambda x: x"), tree)
    True

    No match after switching levels and stacks:
    >>> pat = ast.parse('''
    ... class B:
    ...     def a():
    ...         pass''')
    >>> string = ast.parse('''
    ... class B:
    ...     def a():
    ...         print
    ...     def b():
    ...         pass''')
    >>> match_tree(pat, string)
    False
    """

    # Both iterators should start at the same level
    # with the same stack even if the tree isn't the root node
    start_level = getattr(tree, 'level', 0)
    start_stack = getattr(tree, 'stack', [])

    tree = itervisit(tree, start_level, start_stack)
    pattern = itervisit(pattern, start_level, start_stack)

    m = pattern.next()
    n = tree.next()

    # Should be groups and groupdict if providing an re type interface
    captured = []

    # go through the nodes until we've matched them all or failed
    while True:

        if match_on(m, n):

            # Capture Stuff
            maybe_capture = capture(m, n)
            # If we get a node of fields out of it
            if any(maybe_capture):
                # We don't know if the match is complete, store for later
                captured.append(maybe_capture)

            try:
                m = pattern.next()
            except StopIteration:
                # Matched everything good job
                return captured or True
        else:
            try:
                n = tree.next()
            except StopIteration:
                # Ran out of things to try to match with
                return False
