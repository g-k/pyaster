"""An iterator over the nodes in an AST and assigns level attribute
to each node.
"""

import ast
from itertools import takewhile


def itervisit(node, level=0, stack=[]):
    """Iterates over the nodes of an AST, and
    assigns level and stack attributes to each node.

    Basically NodeVisitor from the stdlib ast module.

    >>> for node in itervisit(ast.parse('a')): # doctest:+ELLIPSIS
    ...     print node
    <_ast.Module object at 0x...>
    <_ast.Expr object at 0x...>
    <_ast.Name object at 0x...>
    <_ast.Load object at 0x...>

    Assigns a tree level (distance from root) to each node. The root is 0:

    >>> itervisit(ast.parse('')).next().level
    0

    Visits children depth first and increments level:

    >>> iv = itervisit(ast.parse('a'))
    >>> module, expr = iv.next(), iv.next()
    >>> expr.level
    1

    Resets level when moving up tree again:

    >>> multilevel = '''
    ... def foo():
    ...     def bar():
    ...         return True
    ...     return bar
    ...
    ... def level_one():
    ...     pass'''
    >>> pass_stmt = [n for n in itervisit(ast.parse(multilevel))][-1]
    >>> pass_stmt.level # inside Module and FunctionDef
    2

    Maintain a stack of the path to each node:

    >>> load = [n for n in itervisit(ast.parse('a'))][-1]
    >>> load.stack                   #doctest:+ELLIPSIS +NORMALIZE_WHITESPACE
    [<_ast.Module object at 0x...>, <_ast.Expr object at 0x...>,
    <_ast.Name object at 0x...>]
    """

    if stack:
        last_level = stack[-1].level
    else:
        last_level = -1  # so the root node gets added as level 0

    if level <= last_level:
        # take nodes from the root level to our node's level
        stack = [n for n in takewhile(lambda n: n.level < level, stack)]

    stack.append(node)

    node.stack = stack[:-1]
    node.level = level
    yield node

    for field, value in ast.iter_fields(node):

        if isinstance(value, list):
            for item in value:

                if isinstance(item, ast.AST):
                    for child_node in itervisit(item, level + 1, stack):
                        yield child_node

        elif isinstance(value, ast.AST):
            for child_node in itervisit(value, level + 1, stack):
                yield child_node
