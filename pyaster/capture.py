
import ast


def capture_node(node):
    """Returns the node if it has a truthy capture attribute.

    Doesn't capture the node if it doesn't have a capture attribute:
    >>> node = ast.Name()
    >>> capture_node(node)
    False

    Captures the node if node.capture is truthy:
    >>> node = ast.Name(); node.capture = True
    >>> capture_node(node)                        #doctest:+ELLIPSIS
    <_ast.Name object at 0x...>

    Doesn't capture the node if node.capture is falsy:
    >>> node = ast.Name(); node.capture = False
    >>> capture_node(node)
    False
    """

    do_capture = getattr(node, 'capture', None)

    if not do_capture:
        return False
    else:
        return node


def capture_fields(node):
    """Captures field values for names given in a capture_fields list
    and returns None if the node doesn't have that field:

    >>> node = ast.Name(id='dog')
    >>> node.capture_fields = ['id', 'dummy_field']
    >>> capture_fields(node) == {'id': 'dog' }
    True
    """

    fields = getattr(node, 'capture_fields', [])

    return dict( (field, getattr(node, field)) for \
                 field in fields if hasattr(node, field))


def capture(pattern, node):
    """Iterates over captured nodes and values from the match pattern
    found in the tree:

    >>> pattern = ast.Name(id='dog', capture=True, capture_fields=['id'])
    >>> capture(pattern, ast.Name(id='dog'))                 #doctest:+ELLIPSIS
    (<_ast.Name object at 0x...>, {'id': 'dog'})

    >>> pattern = ast.Name(id='dog', capture=True)
    >>> capture(pattern, ast.Name(id='dog'))                 #doctest:+ELLIPSIS
    (<_ast.Name object at 0x...>, {})

    >>> pattern = ast.Name(id='dog', capture_fields=['frog'])
    >>> capture(pattern, ast.Name(id='dog'))                 #doctest:+ELLIPSIS
    (None, {})
    """

    captured_nodes = None
    captured_fields = {}

    if hasattr(pattern, 'capture'):
        node.capture = pattern.capture
        captured_nodes = capture_node(node)

    if hasattr(pattern, 'capture_fields'):
        node.capture_fields = pattern.capture_fields
        captured_fields = capture_fields(node)

    return captured_nodes, captured_fields
