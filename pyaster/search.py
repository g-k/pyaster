"""Iterators for finding node patterns in
ASTs, python files, and all python files in a directory and subdirectories
"""

import ast
import itertools
import functools
from multiprocessing import Pool

from visitors import itervisit
from match import match_tree, nodes_match
from helpers import python_filenames, get_node_class, get_node_function


def find_in_tree(pattern, tree, match_on=nodes_match):
    """Returns an iterator over matches in a tree:
    >>> find_in_tree(ast.Print(), ast.parse('print'))  #doctest:+ELLIPSIS
    <generator object find_in_tree at 0x...>

    Returns any matches:
    >>> print_matches = find_in_tree(ast.Print(), ast.parse('print'))
    >>> [print_node for print_node in print_matches]   #doctest:+ELLIPSIS
    [<_ast.Print object at 0x...>]

    Returns nothing for no matches:
    >>> print_matches = find_in_tree(ast.Print(), ast.parse('pringle'))
    >>> [print_node for print_node in print_matches]
    []
    """
    for node in itervisit(tree):
        match = match_tree(pattern, node, match_on)

        if match:
            yield node


def find_in_file(pattern, filename, match_on=nodes_match):
    """Returns an iterator over matches in a file:
    >>> find_in_file(ast.Name(), 'search.py')  #doctest:+ELLIPSIS
    <generator object find_in_file at 0x...>

    Returns any matches:
    >>> first_import = find_in_file(ast.parse('import ast'), 'search.py')
    >>> list(first_import)   #doctest:+ELLIPSIS
    [<_ast.Module object at 0x...>]

    Assigns filename to nodes:
    >>> list(find_in_file(ast.parse('import ast'), 'search.py'))[0].filename
    'search.py'
    """
    with open(filename) as f:
        tree = ast.parse(f.read())

        for node in itervisit(tree):
            node.filename = filename

            match = match_tree(pattern, node, match_on)
            if match:
                yield node


def find_in_directory(pattern, directory, match_on=nodes_match):
    """Returns an iterator over filenames and a list of matchs in them:
    >>> import os
    >>> dir = os.getcwd()
    >>> find_in_directory(ast.parse('import ast'), dir)  #doctest:+ELLIPSIS
    <generator object find_in_directory at 0x...>

    Returns the filename and list of matches in that file:
    >>> import os
    >>> dir = os.getcwd()
    >>> pattern = ast.parse('import ast')
    >>> rs = [(os.path.split(f)[1], m) for f,m in find_in_directory(pattern, dir)]
    >>> file, matches = [ (f,m) for f, m in rs if f == 'search.py'][0]
    >>> file, matches #doctest:+ELLIPSIS
    ('search.py', <_ast.Module object at 0x...>)
    """

    for filename in python_filenames(directory):
        for node in find_in_file(pattern, filename, match_on):
            yield filename, node


def as_list(iterator):
    """Decorator to return results as a list instead of an iterator

    >>> def countdown(tminus):
    ...     while tminus:
    ...         yield tminus
    ...         tminus -= 1
    >>> countdown(10)  #doctest:+ELLIPSIS
    <generator object countdown at 0x...>
    >>> list_down = as_list(countdown)
    >>> list_down(10)
    [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    """
    def listed(*args, **kwargs):
        return list(iterator(*args, **kwargs))
    return listed



## TODO

def pfind_in_directory(pattern, directory, match_on=nodes_match):
    """find_in_directory parallelized over filenames

    Expect the same output as find_in_directory:
    """
    global find_in_file
    izip = itertools.izip
    imap = itertools.imap

    find_in_file = as_list(find_in_file)

    # make a new find function that only takes filename
    find = functools.partial(find_in_file, pattern, match_on=match_on)

    filenames = list(python_filenames(directory))

    for matches in Pool().imap(find, iter(filenames)):
        yield filename, matches


def find_calls(function, directory):
    """finds calls to function in python file in directory
    >>>
    """
    pattern = ast.Call(func=ast.Name(id=function))
    for filename, match in find_in_directory(pattern, directory):
        yield filename, match


def get_call(node, call_dir):
    """Finds calls to the first function in the node's stack
    """

    func = [n for n in reversed(node.stack) if type(n) == ast.FunctionDef][0]
    func_name = func.name

    for filename, node in find_calls(func_name, call_dir):
        yield node, func


def get_calling_class(node, call_dir, max_depth=3):
    """Recursively search function calls to find a class that calls the
    top level function
    """

    if max_depth == 0:
        yield StopIteration

    # if the node is a top level function find out where it's called
    if not get_node_class(node) and get_node_function(node):

        for call, call_to in get_call(node, call_dir):
            yield call, call_to

            for call, call_to in get_calling_class(call, call_dir, max_depth - 1):
                yield call, call_to
