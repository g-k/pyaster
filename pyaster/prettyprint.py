"""Pretty printing with soft tabs and newlines for ASTs"""

import ast

from visitors import itervisit


def pprint_node(node, body_lines=1, indent_with='    '):
    """Returns a node and its fields as a string with soft tabs and newlines
    for pretty printing:

    >>> pprint_node(ast.parse('a()'))                       # doctest:+ELLIPSIS
    'Module body=[<_ast.Expr object at 0x...>]'

    Truncates long body fields at body_lines number of lines:

    >>> pprint_node(ast.parse('a(); b(); c();'), 1)         # doctest:+ELLIPSIS
    "Module body=[<_ast.Expr object at 0x...>, '... 2 more']"

    Indents the node if it has a tree level using the indent_with string:

    >>> a = ast.parse('a')
    >>> a.level = 1
    >>> pprint_node(a)                                      # doctest:+ELLIPSIS
    '    Module body=[<_ast.Expr object at 0x...>]'
    """

    node_type = type(node).__name__

    fields = ''
    for field, value in ast.iter_fields(node):
        if field == 'body' and len(value) > body_lines:
            truncated_lines = '... {0} more'.format(len(value[body_lines:]))
            value = value[:body_lines] + [truncated_lines]

        fields += ' {0}={1}'.format(field, value)

    level = indent_with * getattr(node, 'level', 0)

    return '{level}{node_type}{fields}'.format(**locals())


def pprint_tree(tree, body_lines=1, indent_with='    '):
    """Returns a tree as a string for pretty printing.
    Takes the same options as pprint_node.

    >>> print pprint_tree(ast.parse('a'))                   # doctest:+ELLIPSIS
    Module body=[<_ast.Expr object at 0x...>]
        Expr value=<_ast.Name object at 0x...>
            Name id=a ctx=<_ast.Load object at 0x...>
                Load
    """
    node_strings = [pprint_node(node, body_lines, indent_with) \
                    for node in itervisit(tree)]
    return '\n'.join(node_strings)
