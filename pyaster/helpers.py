"""Mostly untested helper functions
"""

import ast
import fnmatch
import inspect
from itertools import dropwhile
import linecache
import os


def getline(node):
    if hasattr(node, '_attributes'):
        return linecache.getline(node.filename, node.lineno)
    return ''


def debug(node):
    """Return node filename, line number, and source at that location
    """
    return '{0.filename} {0.lineno}\n{1}{2:>{0.col_offset}}\n'\
           .format(node, getline(node), '^')


def python_filenames(directory):
    """Iterate over all python filenames in directory and
    subdirectories ignoring filenames in ignore.

    A special case of: http://www.dabeaz.com/generators/genfind.py
    """
    for (path, names, files) in os.walk(directory):
        for filename in files:
            if fnmatch.fnmatch(filename, '*.py'):
                yield os.path.join(path, filename)


def node_types():
    """A list of all python node types for testing"""

    in_ast_module = (getattr(ast, n) for n in dir(ast))
    return [n for n in in_ast_module \
            if inspect.isclass(n) and issubclass(n, ast.AST)]


def get_body_node(node_with_stack):
    """Returns the body of the first node up the stack that has a body:

    >>> get_body_node(ast.Name(stack=[ast.Module(body=[])])) #doctest:+ELLIPSIS
    <_ast.Module object at 0x...>
    >>> get_body_node(ast.Name(stack=[])) == None
    True
    """
    for node in reversed(node_with_stack.stack):
        if hasattr(node, 'body'):
            return node
    return None


def print_node(node):
    """Prints the node class and function information:
    >>> stack = [ast.ClassDef(name='Bar'), ast.FunctionDef(name='foo')]
    >>> print_node(ast.Name(stack=stack))
    'Bar.foo'

    Returns the empty string if a function or class is not found:
    >>> print_node(ast.Name(stack=[]))
    ''
    """

    return '.'.join([n.name for n in node.stack if type(n) in \
                     (ast.ClassDef, ast.FunctionDef)])


def get_node_class(node):
    """Returns the name of the first class in the node stack"""

    classdef = list(dropwhile(lambda n: type(n) != ast.ClassDef,
                              reversed(node.stack + [node])))

    if classdef:
        classname = classdef[0].name
        return '{0}'.format(classname)
    else:
        return False


def get_node_function(node):
    """Returns the name of the first function in the node stack"""

    functiondef = list(dropwhile(lambda n: type(n) != ast.FunctionDef,
                                 reversed(node.stack + [node])))

    if functiondef:
        functionname = functiondef[0].name
        return '{0}'.format(functionname)
    else:
        return False


def name_node(node):
    """Name the node the first class in its stack otherwise its first function"""

    name = get_node_class(node) or get_node_function(node)

    if name:
        return name
    else:
        print red('Could not find class or function name for node. {0}'.format(node))
        print debug(node)
        return repr(node)
